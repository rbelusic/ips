package com.ips.storage;


import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.fm.FmException;
import org.fm.dm.DmObject;
import org.fm.store.StorageConnection;

/**
 *
 * @author rbelusic
 */
public class RestIpsStorageConnection implements StorageConnection {

    public static void main(String[] args) {
        new RestIpsStorageConnection().find(null, "me", null);
    }

    public Object get(String id) {
        /*
         */

        return null;
    }

    public Object[] get(String[] id) {
        /*
         */

        return null;
    }

    public <T extends DmObject> ArrayList<T> find(Class<T> cls, String qry, Map<String, Object> args) {
        HashMap<String, String> conProps = new HashMap() {
            {
                put("x-test", "test");
            }
        };

        String url = "http://graph.facebook.com/";

        RestIpsStorageResponse result = exec(url + qry, "GET", args, conProps);
        System.out.println(result.getResponseBody());
        return null;
    }

    private String _getBody(Map<String, Object> args) {
        String body = "";
        if (args == null) {
            return body;
        }

        for (Map.Entry<String, Object> e : args.entrySet()) {
            String value = "";
            try {
                value = URLEncoder.encode(e.getValue().toString(), "UTF-8");
            } catch (UnsupportedEncodingException ex) {
                Logger.getLogger(RestIpsStorageConnection.class.getName()).log(Level.SEVERE, null, ex);
            }

            body = body + (body.isEmpty() ? "" : "&") + e.getKey() + "=" + value;
        }

        return body;
    }

    private RestIpsStorageResponse exec(String strUrl, String httpMethod, Map<String, Object> args, Map<String, String> headers) {
        URL url;
        HttpURLConnection connection = null;
        String urlParameters = _getBody(args);

        RestIpsStorageResponse result = new RestIpsStorageResponse();
        result.setTimestamp(System.currentTimeMillis());

        try {
            //Create connection
            url = new URL(strUrl);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod(httpMethod);
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            connection.setRequestProperty("Content-Length", "" + Integer.toString(urlParameters.getBytes().length));
            connection.setRequestProperty("Content-Language", "en-US");

            for (Map.Entry<String, String> h : headers.entrySet()) {
                connection.setRequestProperty(h.getKey(), h.getValue());
            }

            connection.setUseCaches(false);
            connection.setDoInput(true);
            connection.setDoOutput(true);

            //Send request
            DataOutputStream wr = new DataOutputStream(
                    connection.getOutputStream());
            wr.writeBytes(urlParameters);
            wr.flush();
            wr.close();

            //Get Response
            InputStream is = null;
            try {
                is = connection.getInputStream();
            } catch (IOException ex) {
                is = connection.getErrorStream();
            }
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            String line;
            StringBuilder response = new StringBuilder();
            while ((line = rd.readLine()) != null) {
                response.append(line);
                response.append('\n');
            }
            rd.close();

            // construct reponse
            result.setResponseCode(connection.getResponseCode());
            result.setResponseHeaders(connection.getHeaderFields());
            result.setResponseBody(response.toString());
            result.setDuration(System.currentTimeMillis() - result.getTimestamp());
            result.setSuccess(true);
            return result;
        } catch (Exception e) {
            result.setException(e);
            result.setSuccess(true);
            return result;
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
    }

    public void dispose() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public <T extends DmObject> Object serializeDmObject(T dm) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public <T extends DmObject, A> T deserializeDmObject(Class<T> dmObjectClass, Map<String, Object> propSet) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public <T extends DmObject, A> T deserializeDmObject(T dm, Map<String, Object> propSet) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public <T extends DmObject> T get(Class<T> dmObjectClass, Object id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public <T extends DmObject> T get(T dmObject, Object id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public <T extends DmObject> List<T> get(Class<T> dmObjectClass, Object[] ids) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public <T extends DmObject> void put(T dm) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public <T extends DmObject> void put(T[] dm) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public <T extends DmObject> void update(T dm, Set<String> attrs) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public <T extends DmObject> void update(T[] dm, Set<String> attrs) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public <T extends DmObject> void delete(Class<T> dmObjectClass, Object id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public <T extends DmObject> void delete(Class<T> dmObjectClass, Object[] id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public <T extends DmObject> void delete(T dm) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public <T extends DmObject> void delete(T[] dm) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public <T extends DmObject> List<T> findBy(Class<T> dmObjectClass, String attr, Object value) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
