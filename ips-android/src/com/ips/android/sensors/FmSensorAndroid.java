package com.ips.android.sensors;

import android.content.Context;
import static android.content.Context.WINDOW_SERVICE;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.view.Display;
import android.view.Surface;
import android.view.WindowManager;
import java.util.ArrayList;
import java.util.HashMap;
import org.fm.application.FmContext;
import org.fm.client.sensors.FmSensor;
import org.fm.client.sensors.FmSensorEvent;
import org.fm.filters.FmFilter;

/**
 *
 * @author rbelusic
 */
public class FmSensorAndroid extends FmSensor implements SensorEventListener {

    private Display display;

    private final Integer sensorTypeAndroid;
    Sensor androidSensor;
    private int currentAccuracy = SensorManager.SENSOR_STATUS_UNRELIABLE;
    private static HashMap<FmSensor.SENSOR_TYPE, Integer> sensorLinkTable = new HashMap() {
        {
            put(FmSensor.SENSOR_TYPE.MAGNETIC_FIELD, Sensor.TYPE_MAGNETIC_FIELD);
            put(FmSensor.SENSOR_TYPE.ACCELEROMETER, Sensor.TYPE_ACCELEROMETER);
        }
    };

    public FmSensorAndroid(Object owner, FmSensor.SENSOR_TYPE stype) {
        super(owner, stype);
        sensorTypeAndroid = sensorLinkTable.get(getSensorType());
        android.content.Context ctx = (android.content.Context) FmContext.getSystemContext();
        display = ((WindowManager) ctx.getSystemService(WINDOW_SERVICE)).getDefaultDisplay();
    }

    @Override
    public void start() {
        android.content.Context ctx = (android.content.Context) FmContext.getSystemContext();
        SensorManager sensorManager = (SensorManager) ctx.getSystemService(Context.SENSOR_SERVICE);

        if (androidSensor == null && sensorTypeAndroid != null) {
            androidSensor = ((SensorManager) ctx.getSystemService(Context.SENSOR_SERVICE)).
                    getDefaultSensor(sensorTypeAndroid);
        }
        if (androidSensor != null) {
            sensorManager.registerListener(this, androidSensor, SensorManager.SENSOR_DELAY_NORMAL);
        }
    }

    @Override
    public void stop() {
        if (androidSensor != null) {
            android.content.Context ctx = (android.content.Context) FmContext.getSystemContext();
            SensorManager sensorManager = (SensorManager) ctx.getSystemService(Context.SENSOR_SERVICE);
            sensorManager.unregisterListener(this, androidSensor);
        }
    }

    @Override
    public void onSensorChanged(SensorEvent se) {
        FmSensorEvent ev = new FmSensorEvent(getSensorType().toString(), filterSensorValues(se.values), se.accuracy, se.timestamp);
        fixSensorEventRotation(ev);
        fireEvent(FmSensor.EVENT, ev);
    }

    @Override
    public void onAccuracyChanged(Sensor arg0, int arg1) {
        currentAccuracy = arg1;
    }

    private void fixSensorEventRotation(FmSensorEvent se) {
        float vt;
        switch (display.getOrientation()) {
            case Surface.ROTATION_90:
                vt = se.values[1];
                se.values[1] = se.values[0];
                se.values[0] = -1 * vt;
                break;
            case Surface.ROTATION_180:
                se.values[0] *= -1;
                se.values[1] *= -1;
                break;
            case Surface.ROTATION_270:
                vt = se.values[1];
                se.values[1] = -1 * se.values[0];
                se.values[0] = vt;
                break;
        }
    }

}
