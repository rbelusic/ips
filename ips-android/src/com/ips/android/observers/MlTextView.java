/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ips.android.observers;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;
import java.util.UUID;
import org.fm.FmException;
import org.fm.FmObject;
import org.fm.ml.hosts.MlHost;
import org.fm.ml.observers.MlObserver;

/**
 *
 * @author rbelusic
 */
public class MlTextView extends TextView implements MlObserver {
    private final static String namespace="";
    
    private boolean executed = false;
    private Object oldValue=null;

    private MlHost host;

    //props
    private String attributeName;
    private String id;

    public MlTextView(Context context) {
        super(context);
    }

    public MlTextView(Context context, AttributeSet attrs) {        
        super(context, attrs);
        attributeName = attrs.getAttributeValue(namespace, "attribute");
    }

    public MlTextView(Context context, AttributeSet attrs, int defStyle) {        
        super(context, attrs, defStyle);
        attributeName = attrs.getAttributeValue(namespace, "attribute");
    }

    public <H extends MlHost> void run(H h) {
        host = h;
        executed = true;
        update(h);
    }

    public boolean isExecuted() {
        return executed;
    }

    public void dispose() {
        executed = false;
        host = null;
    }

    public <H extends MlHost> void setHost(H h) {
        host = h;
    }

    public MlHost getHost() {
        return (MlHost) host;
    }

    public <H extends MlHost> void update(H caller) {
        if (caller == getHost()) {
            String value = getHost().getDmObject().getAttr(attributeName,"");
            if(oldValue == null || oldValue != value) {
                setText(value);
                oldValue = value;
            }
        }
    }

    public void verify() throws FmException {
        
    }

    public Object getNode() {
        return this;
    }

    public String getID() {
        if (id == null) {
            id = UUID.randomUUID().toString();
        }
        return (id);

    }

    /**
     * @return the attributeName
     */
    private String getAttributeName() {
        return attributeName;
    }

    /**
     * @param attributeName the attributeName to set
     */
    private void setAttributeName(String attributeName) {
        this.attributeName = attributeName;
    }
}
