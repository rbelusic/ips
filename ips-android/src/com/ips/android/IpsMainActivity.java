package com.ips.android;

import android.app.Activity;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.ips.android.view.imagemap.DmGeoPoint;
import com.ips.android.view.imagemap.ImageSurfaceView;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.fm.application.FmContext;
import org.fm.client.sensors.FmSensor;
import org.fm.client.sensors.FmSensorEvent;
import org.fm.filters.FmLowPassFilter;

public class IpsMainActivity extends Activity {

    private ArrayList<FmSensorEvent> events = new ArrayList<FmSensorEvent>();
    private boolean sensorMagneticFieldStarted = false;
    private FmSensor sensorMagneticField = null;
    private boolean sensorAccelerationStarted = false;
    private FmSensor sensorAcceleration = null;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FmContext.setSystemContext(getBaseContext());

        setContentView(R.layout.main);

        final HashMap<String, Object> data = (HashMap<String, Object>) getLastNonConfigurationInstance();
        if (data != null && (Boolean) data.get("eventsStarted")) {
            onStartStop(findViewById(R.id.wgSensors));
        }

        showFloorMap("03520896-388d-4385-8ca7-766e48e98c41", 1);
    }

    @Override
    public Object onRetainNonConfigurationInstance() {
        final HashMap<String, Object> data = new HashMap<String, Object>();
        data.put("eventsStarted", sensorMagneticFieldStarted);
        return data;
    }

    public void onStartStop(View view) {
        startStopSensors(view);
    }

    public void onClear(View view) {
        events.clear();
        updateSensorPanel(null);
    }

    public void onSend(View view) {
        // Do something in response to button
    }

    // FM
    public void onSensorChanged(Object sender, Object evdata) {
        FmSensorEvent se = (FmSensorEvent) evdata;
        if (se.accuracy == SensorManager.SENSOR_STATUS_UNRELIABLE) {
            return;
        }

        if (se.eventType.equals(FmSensor.SENSOR_TYPE.MAGNETIC_FIELD.name())) {
            if (events.size() > 200) {
                events.remove(0);
            }
            events.add(se);
        }

        updateSensorPanel(se);
    }

    // private
    private void updateSensorPanel(FmSensorEvent se) {
        TextView mTxt;
        if (se.eventType.equals(FmSensor.SENSOR_TYPE.MAGNETIC_FIELD.name())) {
            //mag
            mTxt = (TextView) findViewById(R.id.wgSensorDataSideValue);
            mTxt.setText(String.valueOf(se == null ? 0. : String.format("%12.6f", se.values[0])));

            mTxt = (TextView) findViewById(R.id.wgSensorDataFrontValue);
            mTxt.setText(String.valueOf(se == null ? 0. : String.format("%12.6f", se.values[1])));

            mTxt = (TextView) findViewById(R.id.wgSensorDataBelowValue);
            mTxt.setText(String.valueOf(se == null ? 0. : String.format("%12.6f", se.values[2])));

            mTxt = (TextView) findViewById(R.id.wgSensorDataNsamplesValue);
            mTxt.setText(String.valueOf(events.size()));
        } else {
            //acc
            mTxt = (TextView) findViewById(R.id.wgSensorAccSideValue);
            mTxt.setText(String.valueOf(se == null ? 0. : String.format("%12.6f", se.values[0])));

            mTxt = (TextView) findViewById(R.id.wgSensorAccFrontValue);
            mTxt.setText(String.valueOf(se == null ? 0. : String.format("%12.6f", se.values[1])));

            mTxt = (TextView) findViewById(R.id.wgSensorAccBelowValue);
            mTxt.setText(String.valueOf(se == null ? 0. : String.format("%12.6f", se.values[2])));
        }
    }

    private void startStopSensors(View view) {
        if (sensorMagneticField == null) {
            sensorMagneticField = FmContext.getSensorsService(this, FmSensor.SENSOR_TYPE.MAGNETIC_FIELD);
            sensorMagneticField.addListener(this);
            sensorMagneticField.addFilter(new FmLowPassFilter((float) 0.1, 5, 100));
        }
        if (sensorMagneticFieldStarted) {
            sensorMagneticField.stop();
            sensorMagneticFieldStarted = false;
        } else {
            sensorMagneticField.start();
            sensorMagneticFieldStarted = true;
        }

        // acc
        if (sensorAcceleration == null) {
            sensorAcceleration = FmContext.getSensorsService(this, FmSensor.SENSOR_TYPE.ACCELEROMETER);
            sensorAcceleration.addListener(this);
            sensorAcceleration.addFilter(new FmLowPassFilter((float) 0.1, 5, 100));
        }
        if (sensorAccelerationStarted) {
            sensorAcceleration.stop();
            sensorAccelerationStarted = false;
        } else {
            sensorAcceleration.start();
            sensorAccelerationStarted = true;
        }

        Button mButton = (Button) view.findViewById(R.id.btnStartStop);
        mButton.setText(sensorMagneticFieldStarted ? "Stop" : "Start");

    }

    // 03520896-388d-4385-8ca7-766e48e98c41 , 1
    private void showFloorMap(String buildingId, int floorNumber) {
        ImageSurfaceView wgMap = (ImageSurfaceView) findViewById(R.id.wgMap);
        try {
            wgMap.setInputStream(getAssets().open(
                    "buildings/" + buildingId + "/floors/" + floorNumber + ".png"
                    ));
            
            wgMap.setViewportCenter();
            wgMap.addListener(this);
        } catch (IOException ex) {
            Logger.getLogger(IpsMainActivity.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public void onContextMenu(Object owner,Object o) {
        DmGeoPoint g = (DmGeoPoint) o;
        Log.d("MAP", "onContextMenu: (" + g.getX(Double.NaN) + "," + g.getY(Double.NaN) + ")");
    }

    public void onDoubleClick(Object owner,Object o) {
        DmGeoPoint g = (DmGeoPoint) o;
        Log.d("MAP", "onDoubleClick: (" + g.getX(Double.NaN) + "," + g.getY(Double.NaN) + ")");
    }
}
