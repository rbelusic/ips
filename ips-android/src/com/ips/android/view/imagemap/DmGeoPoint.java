/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ips.android.view.imagemap;

import org.fm.dm.DmObject;

/**
 *
 * @author rbelusic
 */
public interface DmGeoPoint extends DmObject {

    public Double getX(Double def);

    public void setX(Double def);

    public Double getY(Double def);

    public void setY(Double def);
}
