INSERT INTO "IPS"."FINGERPRINT_TYPES" (ID,DESCRIPTION) VALUES ('GSM','GSM cell');
INSERT INTO "IPS"."FINGERPRINT_TYPES" (ID,DESCRIPTION) VALUES ('MF','Magnetic field');
INSERT INTO "IPS"."FINGERPRINT_TYPES" (ID,DESCRIPTION) VALUES ('WF','Wi Fi');

INSERT INTO "IPS"."DEVICES" (ID,MODEL,DATA) VALUES ('some_device_id','Android XY','some data');

INSERT INTO "IPS"."USERS" (ID,TITLE,DESCRIPTION) VALUES ('7da8d07c-014f-4549-8a0b-bc5c3ffdb577','Nova promet d.o.o.','Test owner');

INSERT INTO "IPS"."BUILDINGS" (ID,OWNER_ID,TITLE,DESCRIPTION,MAP_URL) VALUES ('03520896-388d-4385-8ca7-766e48e98c41','7da8d07c-014f-4549-8a0b-bc5c3ffdb577','Prodajni centar ','Testni prodajni centar','http://test');

INSERT INTO "IPS"."FLOORS" (ID,BUILDING_ID,FLOOR_NUMBER) VALUES ('0a6c9ee3-4584-428c-8942-d3a2518cab48','03520896-388d-4385-8ca7-766e48e98c41',1);
INSERT INTO "IPS"."FLOORS" (ID,BUILDING_ID,FLOOR_NUMBER) VALUES ('45a5573f-116c-426e-afb5-bd3c8d00bf62','03520896-388d-4385-8ca7-766e48e98c41',2);
INSERT INTO "IPS"."FLOORS" (ID,BUILDING_ID,FLOOR_NUMBER) VALUES ('c9d342a6-5a73-4065-b244-37f0c4bea416','03520896-388d-4385-8ca7-766e48e98c41',0);

INSERT INTO "IPS"."FINGERPRINTS" (ID,XPOS,YPOS,FLOOR_ID,DEVICE_ID,FINGERPRINT_TYPE,FINGERPRINT_VALUE,FINGERPRINT_VALUE_2,FINGERPRINT_VALUE_3,FINGERPRINT_VALUE_4,FINGERPRINT_VALUE_5,FINGERPRINT_VALUE_6,FINGERPRINT_VALUE_7,MODIFIED_AT) VALUES ('2acdf9a1-4d8d-483a-8b6c-2681e32d2e1a',3.0,7.0,'c9d342a6-5a73-4065-b244-37f0c4bea416','some_device_id','MF',77.0,null,null,null,null,null,null,{ts '2014-01-21 13:55:28.164000'});
INSERT INTO "IPS"."FINGERPRINTS" (ID,XPOS,YPOS,FLOOR_ID,DEVICE_ID,FINGERPRINT_TYPE,FINGERPRINT_VALUE,FINGERPRINT_VALUE_2,FINGERPRINT_VALUE_3,FINGERPRINT_VALUE_4,FINGERPRINT_VALUE_5,FINGERPRINT_VALUE_6,FINGERPRINT_VALUE_7,MODIFIED_AT) VALUES ('2f5f18d3-fdac-4a99-a250-e6de78f1bec3',0.0,0.0,'c9d342a6-5a73-4065-b244-37f0c4bea416','some_device_id','MF',100.0,null,null,null,null,null,null,{ts '2014-01-21 13:55:25.612000'});
INSERT INTO "IPS"."FINGERPRINTS" (ID,XPOS,YPOS,FLOOR_ID,DEVICE_ID,FINGERPRINT_TYPE,FINGERPRINT_VALUE,FINGERPRINT_VALUE_2,FINGERPRINT_VALUE_3,FINGERPRINT_VALUE_4,FINGERPRINT_VALUE_5,FINGERPRINT_VALUE_6,FINGERPRINT_VALUE_7,MODIFIED_AT) VALUES ('cc28e7c5-bd23-4e27-af8f-974ae7c8a622',4.0,3.0,'c9d342a6-5a73-4065-b244-37f0c4bea416','some_device_id','MF',44.0,null,null,null,null,null,null,{ts '2014-01-21 13:55:31.148000'});
