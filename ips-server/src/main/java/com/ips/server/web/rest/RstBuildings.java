package com.ips.server.web.rest;

import com.ips.bo.BoBuildings;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;


@Path("/buildings")
public class RstBuildings extends RstObjectImpl {

    @GET
    @Path("{ids}")
    @Produces(APPLICATION_JSON_UTF8)
    public Response getBuildings(@PathParam("ids") String ids) {
        return(
            Response.ok(
                ((BoBuildings)bo(BoBuildings.class)).getBuildings(getIdsArray(ids))
            ).build()
        );
    }
    
    @GET
    @Path("{bid}/floors/{ids: .*}")
    @Produces(APPLICATION_JSON_UTF8)
    public Response getBuildingFloors(@PathParam("bid") String bid, @PathParam("ids") String ids) {
        return(
            Response.ok(
                ((BoBuildings)bo(BoBuildings.class)).getBuildingFloors(bid,getIdsArray(ids))
            ).build()
        );
    }
    
}
