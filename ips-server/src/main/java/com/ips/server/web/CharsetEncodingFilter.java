package com.ips.server.web;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;


public class CharsetEncodingFilter implements Filter {
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) 
    throws IOException, ServletException {
        
        
        System.out.println(servletRequest.getCharacterEncoding());
        servletRequest.setCharacterEncoding("UTF-8");
        servletResponse.setContentType("text/xml;charset=UTF-8");

        
        filterChain.doFilter(servletRequest, servletResponse);
    }

    public void destroy() {
    }
    
}

