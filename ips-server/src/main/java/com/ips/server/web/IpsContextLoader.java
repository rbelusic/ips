package com.ips.server.web;

import com.ips.bo.BoBuildings;
import com.ips.bo.BoDevices;
import com.ips.bo.BoFingerprints;
import com.ips.bo.BoFloors;
import com.ips.bo.BoInterceptor;
import com.ips.bo.BoMeasurements;
import com.ips.bo.BoUsers;
import com.ips.bo.impl.BoBuildingsImpl;
import com.ips.bo.impl.BoDevicesImpl;
import com.ips.bo.impl.BoFingerprintsImpl;
import com.ips.bo.impl.BoFloorsImpl;
import com.ips.bo.impl.BoGraphImpl;
import com.ips.bo.impl.BoMeasurementsImpl;
import com.ips.bo.impl.BoUsersImpl;
import com.ips.dm.DmSession;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import org.fm.FmException;

import org.fm.application.FmApplication;
import org.fm.application.FmContext;
import org.fm.dm.DmFactory;
import org.fm.dm.DmObject;
import org.fm.graph.bo.BoGraph;
import org.fm.utils.SystemUtil;
import org.slf4j.LoggerFactory;

/**
 * Za sada samo ubacujemo servlet config, request i response u context. 
 * TODO:
 provjera tokena, povuci trenutnog usera te ubaciti i njega u GeoEventSession
 *
 */
public class IpsContextLoader implements Filter {
    private static org.slf4j.Logger L = LoggerFactory.getLogger(IpsContextLoader.class);
    
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        L.info("Init");
        // kreiraj novu app        
        ServletContext ctx = filterConfig.getServletContext();        
        String appName = ctx.getContextPath().startsWith("/") ?
                ctx.getContextPath().substring(1) :
                ctx.getContextPath();
        FmApplication app = new FmApplication(appName);
        FmContext.setApplication(app);
        
        app.setBo(BoUsers.class, new BoUsersImpl(), new BoInterceptor());
        app.setBo(BoBuildings.class, new BoBuildingsImpl(), new BoInterceptor());
        app.setBo(BoFloors.class, new BoFloorsImpl(), new BoInterceptor());
        app.setBo(BoFingerprints.class, new BoFingerprintsImpl(), new BoInterceptor());
        app.setBo(BoMeasurements.class, new BoMeasurementsImpl(), new BoInterceptor());
        app.setBo(BoDevices.class, new BoDevicesImpl(), new BoInterceptor());
        app.setBo(BoGraph.class, new BoGraphImpl(), new BoInterceptor());
        
        // dao beans
        //app.setDao(DaoUsers.class, new DaoUsersImpl(), new DaoInterceptor());      
        
        List<Class> clsList = null;
        try {
            clsList = SystemUtil.getClassesFromPackage("com.ips.dm", DmObject.class);
            clsList.addAll(SystemUtil.getClassesFromPackage("org.fm.graph.model", DmObject.class));
            
        } catch (IOException ex) {
            L.error("",ex);
        } catch (URISyntaxException ex) {
            L.error("",ex);
        }
        
        if(clsList != null) for(Class c: clsList) {
            FmContext.addDmClassType(c.getSimpleName(), c);
        }
        
        
    }

    
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        L.info("doFilter");
        // session
        IpsContext appContext = new IpsContext();
        FmContext.setSessionContext(appContext);
        
        appContext.setServletRequest(request);
        appContext.setServletResponse(response);
                        
        
        String reqToken = appContext.getRequestToken();
        DmSession geSession = DmFactory.create(DmSession.class);
        
        if(reqToken != null && !reqToken.isEmpty()) {
            /*
            DaoSystemImpl daoSys = new DaoSystemImpl();
            geSession = daoSys.getSession(reqToken);
            
            if(geSession == null || !geSession.getIp("").equals(appContext.getRequestIp())) {
                throw new FmException("FE0002","Invalid authorisation token.");                
            }
            if(geSession.getClosedAt(null) != null) {
                throw new FmException("FE0002","Authorisation expired.");                
            }
            geSession.setTimestamp(new Date());
            daoSys.updateSession(geSession,null);
            */
            appContext.setSessionInfo(geSession);
        } else {
        }
        // Pass request back down the filter chain
        chain.doFilter(request, response);
        
    }

    @Override
    public void destroy() {
        L.info("destroy");
    }

}
