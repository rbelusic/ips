package com.ips.server.web.rest;

import com.ips.bo.BoUsers;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

@Path("/users")
public class RstUsers extends RstObjectImpl {

    @GET
    @Path("{ids}")
    @Produces(APPLICATION_JSON_UTF8)
    public Response getUsers(@PathParam("ids") String ids) {        
        return(
            Response.ok(
                ((BoUsers)bo(BoUsers.class)).getUsers(getIdsArray(ids))
            ).build()
        );
    }
    
    @GET
    @Path("{uid}/buildings/{ids: .*}")
    @Produces(APPLICATION_JSON_UTF8)
    public Response getUserBuildings(@PathParam("uid") final String uid, @PathParam("ids") String ids) {
        return(
            Response.ok(
                ((BoUsers)bo(BoUsers.class)).getBuildings(uid,getIdsArray(ids))
            ).build()
        );
    }
    
}
