package com.ips.server.web.rest;

import com.ips.bo.BoFloors;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

@Path("/floors")
public class RstFloors extends RstObjectImpl {

    @GET
    @Path("{ids}")
    @Produces(APPLICATION_JSON_UTF8)
    public Response getFloors(@PathParam("ids") String ids) {        
        return(
            Response.ok(
                ((BoFloors)bo(BoFloors.class)).getFloors(getIdsArray(ids))
            ).build()
        );
    }
    
    @GET
    @Path("{fid}/fingerprints/{ids: .*}")
    @Produces(APPLICATION_JSON_UTF8)
    public Response getFloorFingerprints(@PathParam("fid") String fid, @PathParam("ids") String ids, 
            @QueryParam("xmin") Double xmin,
            @QueryParam("ymin") Double ymin,
            @QueryParam("xmax") Double xmax,
            @QueryParam("ymax") Double ymax    
        ) {
        return(
            Response.ok(
                ((BoFloors)bo(BoFloors.class)).getFloorFingerprints(fid,getIdsArray(ids),xmin,ymin,xmax, ymax)
            ).build()
        );
    }
    
}
