package com.ips.server.web.rest;

import com.ips.bo.BoMeasurements;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

@Path("/measurements")
public class RstMeasurements extends RstObjectImpl {

    @GET
    @Path("{ids}")
    @Produces(APPLICATION_JSON_UTF8)
    public Response getMeasurements(@PathParam("ids") String ids) {        
        return(
            Response.ok(
                ((BoMeasurements)bo(BoMeasurements.class)).getMeasurements(getIdsArray(ids))
            ).build()
        );
    }
    
}
