package com.ips.server.web.rest;

import com.ips.server.web.IpsContext;
import org.fm.application.FmContext;
import org.fm.bo.BoObject;
import org.fm.rest.FmRestService;

public class RstObjectImpl extends FmRestService {
    public final IpsContext ctx = FmContext.getSessionContext();    
    
    public <T extends BoObject> T bo(Class<T> c) {
        return FmContext.getApplication().getBo(c);
    }
}
