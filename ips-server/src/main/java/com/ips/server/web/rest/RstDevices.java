package com.ips.server.web.rest;

import com.ips.bo.BoDevices;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

@Path("/devices")
public class RstDevices extends RstObjectImpl {

    @GET
    @Path("{ids}")
    @Produces(APPLICATION_JSON_UTF8)
    public Response getDevices(@PathParam("ids") String ids) {        
        return(
            Response.ok(
                ((BoDevices)bo(BoDevices.class)).getDevices(getIdsArray(ids))
            ).build()
        );
    }
}
