package com.ips.server.web.rest;

import com.ips.bo.BoFingerprints;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

@Path("/fingerprints")
public class RstFingerprints extends RstObjectImpl {

    @GET
    @Path("{ids}")
    @Produces(APPLICATION_JSON_UTF8)
    public Response getFingerprints(@PathParam("ids") String ids) {        
        return(
            Response.ok(
                ((BoFingerprints)bo(BoFingerprints.class)).getFingerprints(getIdsArray(ids))
            ).build()
        );
    }
    
    @GET
    @Path("types")
    @Produces(APPLICATION_JSON_UTF8)
    public Response getFingerprintTypes() {        
        return(
            Response.ok(
                ((BoFingerprints)bo(BoFingerprints.class)).getFingerprintTypes()
            ).build()
        );
    }
    
    
    @GET
    @Path("{fgid}/measurements/{ids: .*}")
    @Produces(APPLICATION_JSON_UTF8)
    public Response getFingerprintMeasurements(@PathParam("fgid") String fgid, @PathParam("ids") String ids) {
        return(
            Response.ok(
                ((BoFingerprints)bo(BoFingerprints.class)).getFingerprintMeasurements(fgid,getIdsArray(ids))
            ).build()
        );
    }
    
}
