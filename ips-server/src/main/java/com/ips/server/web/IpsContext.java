package com.ips.server.web;

import com.ips.dm.DmSession;
import java.util.List;
import java.util.UUID;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import org.fm.FmException;
import org.fm.application.FmContext;
import org.fm.dm.DmFactory;
import org.fm.store.Storage;
import org.fm.store.StorageConnection;
import org.slf4j.LoggerFactory;

public class IpsContext {
    private static org.slf4j.Logger L = LoggerFactory.getLogger(IpsContext.class);
    private DmSession sessionInfo;
    private StorageConnection storageConnection;
    private boolean storageConnectionLoaded = false;
    
    private ServletRequest servletRequest;
    private ServletResponse servletResponse;
//    private DmSession sessionInfo;
//    private List<DmRole> userRoles = null;

    public <T extends StorageConnection> StorageConnection getStorageConnection() {
        if (!storageConnectionLoaded) {
            storageConnectionLoaded = true;
            Class<T> conClass = null;
            
            try {
                String driver = FmContext.getProperty("fm.store.driver");
                conClass = (Class<T>) Class.forName(driver);

            } catch (Exception ex) {
                throw new FmException("FEM001", "Driver not found.", ex);
            }
            storageConnection = Storage.instance(conClass);
        }

        return storageConnection;
    }
    
    /**
     * @return the servletRequest
     */
    public ServletRequest getServletRequest() {
        return servletRequest;
    }

    /**
     * @param servletRequest the servletRequest to set
     */
    public void setServletRequest(ServletRequest servletRequest) {
        this.servletRequest = servletRequest;
    }

    /**
     * @return the servletResponse
     */
    public ServletResponse getServletResponse() {
        return servletResponse;
    }

    /**
     * @param servletResponse the servletResponse to set
     */
    public void setServletResponse(ServletResponse servletResponse) {
        this.servletResponse = servletResponse;
    }

    public String getRequestToken() {
        return (((HttpServletRequest) getServletRequest()).getHeader("X-ips-token"));
    }

    public String getnerateRequestToken() {
        return (UUID.randomUUID().toString());
    }

    public String getRequestIp() {
        return (((HttpServletRequest) getServletRequest()).getRemoteAddr());
    }

    public DmSession getSessionInfo() {
        return this.sessionInfo;
    }

    public void setSessionInfo(DmSession ses) {
        this.sessionInfo = ses;
        /*
        if (this.sessionInfo != null) {
            userRoles = new DaoUsersImpl().getRoles(ses.getUserId(""));
        } else {
            userRoles = null;
        }*/
    }

    public boolean isSessionAuthenticated() {
        return (sessionInfo == null);
    }

    public String getUserId() {
        return getSessionInfo() == null ? "" : getSessionInfo().getUserId("");
    }
    
    // -- roles ----------------------------------------------------------------
        public boolean checkUserRole(String roleid) {
            return true;
        }
/*
    public List<DmRole> getUserRoles(String userId) {
        return userRoles;
    }

    public boolean checkUserRole(String roleid) {
        if (userRoles == null || getSessionInfo() == null) {
            return false;
        }

        for (DmRole role : userRoles) {
            if (role.getId("").equalsIgnoreCase(roleid)) {
                return true;
            }
        }
        return false;
    }


    public static DmSysInfo getBuildInfo() {
        DmSysInfo info = DmFactory.create(DmSysInfo.class);
        info.setName(FmContext.getProperty("build.project.name"));
        info.setVersion(FmContext.getProperty("build.project.version"));
        info.setBuildTime(FmContext.getProperty("build.project.time"));

        return (info);

    }
    */
}
