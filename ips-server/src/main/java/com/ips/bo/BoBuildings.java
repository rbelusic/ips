package com.ips.bo;

import com.ips.dm.IpsBuilding;
import com.ips.dm.IpsFloor;
import java.util.List;
import org.fm.bo.BoObject;

/**
 *
 * @author RobertoB
 */
public interface BoBuildings extends BoObject {
    public List<IpsBuilding> getBuildings(String [] ids);
    
    public List<IpsFloor> getBuildingFloors(final String buildingId, String[] ids);
}
