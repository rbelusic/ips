package com.ips.bo;

import com.ips.dm.IpsBuilding;
import com.ips.dm.IpsUser;
import java.util.List;
import org.fm.bo.BoObject;

/**
 *
 * @author RobertoB
 */
public interface BoUsers extends BoObject {
    public List<IpsUser> getUsers(String [] ids);
    
    public List<IpsBuilding> getBuildings(final String uid, String[] ids);
}
