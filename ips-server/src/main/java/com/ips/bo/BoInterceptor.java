package com.ips.bo;

import com.ips.server.web.IpsContext;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.fm.FmException;
import org.fm.application.FmContext;
import org.fm.proxy.GenericInterceptor;
import org.fm.bo.annotations.BoRoles;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BoInterceptor implements GenericInterceptor {
    private static Logger L = LoggerFactory.getLogger(BoInterceptor.class);
    private Map<Method,List<String>> roles = new HashMap<Method, List<String>>();
    
    private static String getMethodName(Method method) {
        return method.getDeclaringClass().getSimpleName() + "." + method.getName();
    }
    
    @Override
    public void before(Method method, Object[] args) {
        L.debug("(before) {}({}) ", getMethodName(method), args);
        if(!checkMethodAthorisation(method)) {        
            throw new FmException("Not autorized");
        }
    }

    @Override
    public void after(Method method, Object[] args, Object response) {      
        if(response instanceof Throwable) {
            L.error("Error excetuting " + getMethodName(method) + ":", (Throwable)response);
        }
        L.debug("(after) {}({}) = {} ", new Object[] {getMethodName(method),(args), response});
        
        // log errors here, we don't hawe proxy on rst level
        
    }
        
    // priv
    private List<String> getMethodRoles(Method m) {
        if(roles.get(m) == null) {
            BoRoles ann = (BoRoles)m.getAnnotation(BoRoles.class);
            if(ann != null) {
                roles.put(m, Arrays.asList(ann.roles()));
            }
            roles.put(m, new ArrayList<String>());
        }
        
        return(roles.get(m));
        
    }
    
    private boolean checkMethodAthorisation(Method method) {
        List<String> mroles = getMethodRoles(method);
        if(mroles.size() < 1) {
            return true;
        }
        
        IpsContext ctx = (IpsContext)FmContext.getSessionContext();        
        for(String rname: getMethodRoles(method)) {
            if(ctx.checkUserRole(rname)) return true;                
        }
        
        return false;
    }
}
