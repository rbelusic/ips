package com.ips.bo;

import com.ips.dm.IpsFingerprint;
import com.ips.dm.IpsFloor;
import java.util.List;
import org.fm.bo.BoObject;

/**
 *
 * @author RobertoB
 */
public interface BoFloors extends BoObject {
    public List<IpsFloor> getFloors(String [] ids);
    
    public List<IpsFingerprint> getFloorFingerprints(String floorId, String[] idsArray);

    public List<IpsFingerprint> getFloorFingerprints(String floorId, String[] idsArray, 
            Double xmin, Double ymin, Double xmax, Double ymax);
}
