package com.ips.bo;

import com.ips.dm.IpsDevice;
import java.util.ArrayList;
import java.util.List;
import org.fm.bo.BoObject;

/**
 *
 * @author RobertoB
 */
public interface BoDevices extends BoObject {
    public List<IpsDevice> getDevices(String[] ids);
}
