package com.ips.bo;

import com.ips.dm.IpsMeasurement;
import java.util.List;
import org.fm.bo.BoObject;

/**
 *
 * @author RobertoB
 */
public interface BoMeasurements extends BoObject {
    public List<IpsMeasurement> getMeasurements(String [] ids);
}
