package com.ips.bo;

import com.ips.dm.IpsFingerprint;
import com.ips.dm.IpsFingerprintType;
import com.ips.dm.IpsMeasurement;
import java.util.List;
import org.fm.bo.BoObject;

/**
 *
 * @author RobertoB
 */
public interface BoFingerprints extends BoObject {
    public List<IpsFingerprint> getFingerprints(String [] ids);
    
    public List<IpsFingerprintType> getFingerprintTypes();
    
    public List<IpsMeasurement> getFingerprintMeasurements(String fingerprintId, String[] ids);
}
