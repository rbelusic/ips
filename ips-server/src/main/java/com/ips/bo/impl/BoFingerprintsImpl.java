package com.ips.bo.impl;

import com.ips.bo.BoFingerprints;
import com.ips.dm.IpsFingerprint;
import com.ips.dm.IpsFingerprintType;
import com.ips.dm.IpsMeasurement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.fm.dm.DmFactory;
import org.fm.store.Storage;

/**
 *
 * @author RobertoB
 */
public class BoFingerprintsImpl extends BoObjectImpl implements BoFingerprints {

    @Override
    public List<IpsFingerprint> getFingerprints(String[] ids) {
        List<IpsFingerprint> dmList = 
                ctx().getStorageConnection().get(IpsFingerprint.class, ids);
        return dmList;
    }

    @Override
    public List<IpsMeasurement> getFingerprintMeasurements(String fingerprintId, String[] ids) {
        final String fgId = fingerprintId;
        final String [] idarr = ids;
        
        String qry = "SELECT * FROM " + Storage.getDbCollectionName(IpsMeasurement.class)
                + "  WHERE 1=1 "
                + (ids == null || ids.length == 0
                ? ""
                : " AND " + DmFactory.getDmDataIdAttribute(IpsMeasurement.class) + " IN ([:ids])")
                + " AND fingerprint_id = [:fgid]";

        List<IpsMeasurement> dmList
                = ctx().getStorageConnection().find(
                        IpsMeasurement.class, qry, new HashMap<String, Object>() {
                            {
                                put("ids", idarr);
                                put("fgid", fgId);
                            }
                        }
                );


        return dmList;
    }

    @Override
    public List<IpsFingerprintType> getFingerprintTypes() {
        List<IpsFingerprintType> dmList = 
                ctx().getStorageConnection().get(IpsFingerprintType.class, (String [])null);
        return dmList;        
    }
}
