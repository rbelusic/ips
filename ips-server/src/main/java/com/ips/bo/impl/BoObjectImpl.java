
package com.ips.bo.impl;

import com.ips.server.web.IpsContext;
import org.fm.application.FmContext;
import org.fm.bo.BoObject;
import org.fm.dao.DaoObject;

public class BoObjectImpl implements BoObject {    
    @Override
    public String getID() {
        return(getClass().getSimpleName() + "-ID:" + Thread.currentThread().getId());
        
    }
    
    public IpsContext ctx() {
        return FmContext.getSessionContext();
    }
        /*
    public <T extends DaoObject> T dao(Class<T> c) {
        return FmContext.getApplication().getDao(c);
    }*/
}
