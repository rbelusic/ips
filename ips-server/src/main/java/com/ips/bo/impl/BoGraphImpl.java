package com.ips.bo.impl;

import java.util.HashMap;
import java.util.List;
import org.fm.graph.bo.BoGraph;
import org.fm.graph.model.DmGraph;
import org.fm.graph.model.DmGraphEdge;
import org.fm.graph.model.DmGraphVertex;

public class BoGraphImpl extends BoObjectImpl implements BoGraph {
    @Override
    public <G extends DmGraph> G getGraph(Class<G> cls,String id) {
        return ctx().getStorageConnection().get(
            cls, id
        );
    }

    @Override
    public <V extends DmGraphVertex> V getVertex(Class<V> cls, String id) {
        return ctx().getStorageConnection().get(
                cls, 
                id
        );
    }

    @Override
    public <E extends DmGraphEdge> E getEdge(Class<E> cls,String id) {
        return ctx().getStorageConnection().get(
                cls, 
                id
        );
    }

    @Override
        public <E extends DmGraphEdge,V extends DmGraphVertex> 
            void loadVertexEdges(Class<E> cls,V v) {
        final V vtx = v;
        String qry = "select id, fromNode, toNode, direction from g_edges where "
                + "graphid = [:gid] and (fromNode = [:id] or toNode = [:id])";
        
        List<E> dmList
            = ctx().getStorageConnection().find(
                    cls, qry, new HashMap<String, Object>() {
                        {
                            put("id", vtx.getDataID());
                            put("gid", vtx.getGraphId(""));
                        }
                    }
            );

        vtx.setEdges(dmList);
    }
    
    
}
