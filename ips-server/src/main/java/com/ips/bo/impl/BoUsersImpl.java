package com.ips.bo.impl;

import com.ips.bo.BoUsers;
import com.ips.dm.IpsBuilding;
import com.ips.dm.IpsUser;
import java.util.HashMap;
import java.util.List;
import org.fm.dm.DmFactory;
import org.fm.store.Storage;

/**
 *
 * @author RobertoB
 */
public class BoUsersImpl extends BoObjectImpl implements BoUsers {
    /**
     * Get list of users
     * 
     * @param ids
     * @return 
     */
    @Override
    public List<IpsUser> getUsers(String[] ids) {
        List<IpsUser> dmList = 
                ctx().getStorageConnection().get(IpsUser.class, ids);
        return dmList;
    }

    @Override
    public List<IpsBuilding> getBuildings(final String uid, String[] ids) {
        final String userId = uid;
        final String [] idarr = ids;
        
        String qry = "SELECT * FROM " + Storage.getDbCollectionName(IpsBuilding.class)
                + "  WHERE 1=1 "
                + (ids == null || ids.length == 0
                ? ""
                : " AND " + DmFactory.getDmDataIdAttribute(IpsBuilding.class) + " IN ([:ids])")
                + " AND owner_id = [:uid]";

        List<IpsBuilding> dmList
                = ctx().getStorageConnection().find(
                        IpsBuilding.class, qry, new HashMap<String, Object>() {
                            {
                                put("ids", idarr);
                                put("uid", userId);
                            }
                        }
                );


        return dmList;
    }
}
