/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ips.bo.impl;

import com.ips.bo.BoBuildings;
import com.ips.dm.IpsBuilding;
import com.ips.dm.IpsFloor;
import java.util.HashMap;
import java.util.List;
import org.fm.dm.DmFactory;
import org.fm.store.Storage;


/**
 *
 * @author RobertoB
 */
public class BoBuildingsImpl extends BoObjectImpl implements BoBuildings {

    @Override
    public List<IpsBuilding> getBuildings(String[] ids) {
        List<IpsBuilding> dmList = 
                ctx().getStorageConnection().
                        get(IpsBuilding.class, ids);
        return dmList;
    }

    @Override
    public List<IpsFloor> getBuildingFloors(String buildingId, String[] ids) {
        final String bId = buildingId;
        final String [] idarr = ids;
        
        String qry = "SELECT * FROM " + Storage.getDbCollectionName(IpsFloor.class)
                + "  WHERE 1=1 "
                + (ids == null || ids.length == 0
                ? ""
                : " AND " + DmFactory.getDmDataIdAttribute(IpsFloor.class) + " IN ([:ids])")
                + " AND building_id = [:bid]";

        List<IpsFloor> dmList
                = ctx().getStorageConnection().find(
                        IpsFloor.class, qry, new HashMap<String, Object>() {
                            {
                                put("ids", idarr);
                                put("bid", bId);
                            }
                        }
                );


        return dmList;
    }
}
