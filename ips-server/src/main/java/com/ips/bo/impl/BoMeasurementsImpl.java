package com.ips.bo.impl;

import com.ips.bo.BoMeasurements;
import com.ips.dm.IpsMeasurement;
import java.util.List;

public class BoMeasurementsImpl extends BoObjectImpl implements BoMeasurements {

    @Override
    public List<IpsMeasurement> getMeasurements(String[] ids) {
        List<IpsMeasurement> dmList = 
                ctx().getStorageConnection().get(IpsMeasurement.class, ids);
        return dmList;
    }
}
