package com.ips.bo.impl;

import com.ips.bo.BoFloors;
import com.ips.dm.IpsFingerprint;
import com.ips.dm.IpsFloor;
import java.util.HashMap;
import java.util.List;
import org.fm.dm.DmFactory;
import org.fm.store.Storage;

/**
 *
 * @author RobertoB
 */
public class BoFloorsImpl extends BoObjectImpl implements BoFloors {

    @Override
    public List<IpsFloor> getFloors(String[] ids) {
        List<IpsFloor> dmList = 
                ctx().getStorageConnection().get(IpsFloor.class, ids);
        return dmList;
    }

    @Override
    public List<IpsFingerprint> getFloorFingerprints(String floorId, String[] idsArray) {
        return getFloorFingerprints(floorId, idsArray, null, null, null, null);
    }

    @Override
    public List<IpsFingerprint> getFloorFingerprints(String floorId, String[] idsArray, 
            Double xmin, Double ymin, Double xmax, Double ymax) {
        final String fId = floorId;
        final String [] ids = idsArray;
        
        String qry = "SELECT * FROM " + Storage.getDbCollectionName(IpsFingerprint.class)
                + "  WHERE 1=1 "
                + (ids == null || ids.length == 0
                ? ""
                : " AND " + DmFactory.getDmDataIdAttribute(IpsFingerprint.class) + " IN ([:ids])")
                + " AND floor_id = [:fid]"
                + (xmin != null ? " AND xpos >= [:xmin]" : "")
                + (ymin != null ? " AND ypos >= [:ymin]" : "")
                + (xmax != null ? " AND xmax <= [:xmax]" : "")
                + (ymax != null ? " AND ymax <= [:ymax]" : "")
                ;

        List<IpsFingerprint> dmList
                = ctx().getStorageConnection().find(
                        IpsFingerprint.class, qry, new HashMap<String, Object>() {
                            {
                                put("ids", ids);
                                put("fid", fId);
                            }
                        }
                );


        return dmList;
        
    }
    
}
