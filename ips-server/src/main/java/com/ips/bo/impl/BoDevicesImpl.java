package com.ips.bo.impl;

import com.ips.bo.BoDevices;
import com.ips.dm.IpsDevice;
import java.util.ArrayList;
import java.util.List;
import org.fm.application.FmContext;

/**
 *
 * @author RobertoB
 */
public class BoDevicesImpl extends BoObjectImpl implements BoDevices {
    @Override
    public List<IpsDevice> getDevices(String[] ids) {
        List<IpsDevice> dmList = 
                ctx().getStorageConnection().get(IpsDevice.class, ids);
        return dmList;
    }
}
