package com.ips.dm;

import javax.persistence.Table;
import org.fm.dm.DmObject;
import org.fm.dm.annotations.DmDataIdAttribute;

@DmDataIdAttribute(name = "id")
@Table(name = "fingerprint_types")
public interface IpsFingerprintType extends DmObject {
   public String getId(String def);
   public void setId(String def);
    
   public String getDescription(String def);
   public void setDescription(String def);
}
