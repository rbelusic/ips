package com.ips.dm;

import javax.persistence.Table;
import org.fm.dm.DmObject;
import org.fm.dm.annotations.DmDataIdAttribute;

@DmDataIdAttribute(name = "id")
@Table(name = "fingerprints")
public interface IpsFingerprint extends DmObject {
   public String getId(String def);
   public void setId(String def);
    
   public String getFloor_Id(String def);
   public void setFloor_Id(String def);
    
   public String getDevice_Id(String def);
   public void setDevice_Id(String def);
    
   public String getFingerprint_Type(String def);
   public void setFingerprint_Type(String def);
    
   public Double getXpos(Double def);
   public Double setXpos(Double def);    

   public Double getYpos(Double def);
   public Double setYpos(Double def);    

   public Double getFingerprint_Value(Double def);
   public Double setFingerprint_Value(Double def);    

   public Double getFingerprint_Value_2(Double def);
   public Double setFingerprint_Value_2(Double def);    

   public Double getFingerprint_Value_3(Double def);
   public Double setFingerprint_Value_3(Double def);    

   public Double getFingerprint_Value_4(Double def);
   public Double setFingerprint_Value_4(Double def);    

   public Double getFingerprint_Value_5(Double def);
   public Double setFingerprint_Value_5(Double def);    

   public Double getFingerprint_Value_6(Double def);
   public Double setFingerprint_Value_6(Double def);    

   public Double getFingerprint_Value_7(Double def);
   public Double setFingerprint_Value_7(Double def);    

   public Long getModified_At(Long def);
   public Long setModified_At(Long def);
}
