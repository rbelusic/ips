package com.ips.dm;

import javax.persistence.Table;
import org.fm.dm.DmObject;
import org.fm.dm.annotations.DmDataIdAttribute;

@DmDataIdAttribute(name = "id")
@Table(name = "floors")
public interface IpsFloor extends DmObject {
   public String getId(String def);
   public void setId(String def);
    
   public String getBuilding_Id(String def);
   public void setBuilding_Id(String def);
    
   public Integer getFloor_Number(String def);
   public void setFloor_Number(String def);    
}
