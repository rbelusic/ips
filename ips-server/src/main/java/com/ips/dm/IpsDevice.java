package com.ips.dm;

import javax.persistence.Table;
import org.fm.dm.DmObject;

@Table(name="devices")
public interface IpsDevice extends DmObject {
   public String getId(String def);
   public void setId(String def);
    
   public String getModel(String def);
   public void setModel(String def);
    
   public String getData(String def);
   public void setData(String def);
    
}
