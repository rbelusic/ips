package com.ips.dm;

import javax.persistence.Table;
import org.fm.graph.model.DmGraphVertex;


@Table(name = "g_nodes")
public interface IpsGraphVertex extends DmGraphVertex {
}
