package com.ips.dm;

import javax.persistence.Table;
import org.fm.dm.DmObject;
import org.fm.dm.annotations.DmDataIdAttribute;

@DmDataIdAttribute(name = "id")
@Table(name = "buildings")
public interface IpsBuilding extends DmObject {
   public String getId(String def);
   public void setId(String def);
    
   public String getOwner_Id(String def);
   public void setOwner_Id(String def);
    
   public String getTitle(String def);
   public void setTitle(String def);
    
   public String getDescription(String def);
   public void setDescription(String def);
    
   public String getMap_url(String def);
   public void setMap_url(String def);    
}
