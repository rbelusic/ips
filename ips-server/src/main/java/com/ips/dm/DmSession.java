package com.ips.dm;

import java.util.Date;
import javax.persistence.Table;
import org.fm.dm.DmObject;
import org.fm.dm.annotations.DmDataIdAttribute;

@DmDataIdAttribute(name = "id")
@Table(name = "sessions")
public interface DmSession extends DmObject {
    public String getId(String def);
    public void setId(String def);

    public String getUserId(String def);
    public void setUserId(String def);

    public String getIp(String def);
    public void setIp(String def);

    public Date getTimestamp(Date def);
    public void setTimestamp(Date def);

    public Date getClosedAt(Date def);
    public void setClosedAt(Date def);
}
