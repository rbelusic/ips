package com.ips.dm;

import javax.persistence.Table;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.fm.graph.model.DmGraphEdge;

@JsonSerialize(using=DmSerializer.class)
@Table(name = "g_edges")
public interface IpsGraphEdge extends DmGraphEdge {
}
