package com.ips.dm;

import javax.persistence.Table;
import org.fm.graph.model.DmGraph;

@Table(name = "g_graph")
public interface IpsGraph extends DmGraph {
   public String getName(String def);
   public void setName(String def);
    
}
