package com.ips.dm;

import javax.persistence.Table;
import org.fm.dm.DmObject;
import org.fm.dm.annotations.DmDataIdAttribute;

@DmDataIdAttribute(name = "id")
@Table(name = "measurements")
public interface IpsMeasurement extends DmObject {
   public String getId(String def);
   public void setId(String def);
    
   public String getFingerprint_Type(String def);
   public void setFingerprint_Type(String def);
    
   public String getFingerprint_Id(String def);
   public void setFingerprint_Id(String def);
    
   public Double getXpos(Double def);
   public Double setXpos(Double def);    

   public Double getYpos(Double def);
   public Double setYpos(Double def);    

   public String getData(String def);
   public void setData(String def);
   
   public Long getInserted_At(Long def);
   public Long setInserted_At(Long def);
   
   public Long getProcessed_At(Long def);
   public Long setProcessed_At(Long def);
   
}
