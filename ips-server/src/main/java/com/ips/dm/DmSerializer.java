/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ips.dm;

import java.io.IOException;
import java.util.HashMap;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.JsonSerializer;
import org.codehaus.jackson.map.SerializerProvider;
import org.fm.dm.DmFactory;
import org.fm.dm.DmObject;

/**
 *
 * @author RobertoB
 */
public class DmSerializer extends JsonSerializer<DmObject> {

    @Override
    public void serialize(DmObject dm, JsonGenerator jg, SerializerProvider sp) throws IOException, JsonProcessingException {
        if (dm != null) {
            jg.writeStartObject();
            for(String key: dm.getAttributeNames()) {
                Object value = dm.getAttr(key,null);
                jg.writeFieldName( key );
                if(value == null) {
                    jg.writeNull();
                } else {
                    jg.writeObject( value );
                }                
            }
            jg.writeFieldName("@dmKind");
            jg.writeString(DmFactory.getDmKind(dm.getSubClass()));

            jg.writeEndObject();
        } else {
            jg.writeNull();
        }

    }

}
