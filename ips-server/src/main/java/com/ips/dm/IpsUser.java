package com.ips.dm;

import javax.persistence.Table;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.fm.dm.DmObject;
import org.fm.dm.annotations.DmDataIdAttribute;

@DmDataIdAttribute(name = "id")
@Table(name = "users")
public interface IpsUser extends DmObject {
    public String getId(String def);
    public void setId(String def);

    public String getTitle(String def);
    public void setTitle(String def);

    public String getDescription(String def);
    public void setDescription(String def);
}
