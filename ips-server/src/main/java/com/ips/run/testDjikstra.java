/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ips.run;

import com.ips.bo.impl.BoGraphImpl;
import com.ips.dm.IpsGraphEdge;
import com.ips.dm.IpsGraphVertex;
import java.util.ArrayList;
import org.fm.application.FmContext;
import org.fm.graph.Graph;
import static org.fm.graph.algol.Dijkstra.computeShortestPath;
import org.fm.graph.bo.BoGraph;
import org.fm.graph.model.DmGraphVertex;

/**
 *
 * @author rbelusic
 */
public class testDjikstra {

/*
 2----------------6
 |                |
 |                |
 |                |
 9 ----0-------1---------
 |             |        |
 |             |        |
 |             |        |
 |             |        |
 |-----4-------|        5 ------3
 |
 |
 |
 7------8
*/

    public static void main(String[] args) {
        
        ArrayList<DmGraphVertex> fndPath = computeShortestPath(
                new Graph("graph-test", IpsGraphVertex.class, IpsGraphEdge.class),
                "graph-test-n4", "graph-test-n3"
                );

        int step=1;
        for (DmGraphVertex v: fndPath) {
            System.out.println(" " + step + " =>" + v.getDataID());
            step++;
        }
        
    }     
}
